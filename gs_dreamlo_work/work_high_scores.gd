#
# Copyright 2018-2021 SpockerDotNet LLC, All Rights Reserved.
#

extends VBoxContainer

@onready var score_box = preload("res://gs_dreamlo_work/work_score.tscn")

func add_score(score):
	var sb = score_box.instantiate()
	sb.text = "{name} : {score}".format({ "name": score.name, "score": score.score })
	add_child(sb)
