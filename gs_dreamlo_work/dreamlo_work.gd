#
# Copyright 2018-2021 SpockerDotNet LLC, All Rights Reserved.
#

extends Node

@onready var message = get_node("Container/_message")
@onready var high_scores = get_node("Container/HighScores")

@export var config_filename : String

func _ready():
	DreamLo.load_settings(config_filename)
	DreamLo.clear_scores()
	DreamLo.add_score("player1", 1000, 1)
	DreamLo.add_score("player2", 2000, 2)
	DreamLo.add_score("player3", 3000, 3)
	DreamLo.add_score("player4", 4000, 4)
	DreamLo.add_score("player5", 5000, 5)
	DreamLo.add_score("player6", 6000 ,6)
	DreamLo.get_scores()


func _on_clearing_scores():
	message.text = "clearing scores"


func _on_cleared_scores():
	message.text = "scores cleared"


func _on_getting_scores():
	message.text = "getting scores"


func _on_got_scores(scores):
	message.text = ""
	for score in scores:
		high_scores.add_score(score)


func _init():
	DreamLo.dreamlo_cleared_scores.connect(_on_clearing_scores)
	DreamLo.dreamlo_cleared_scores.connect(_on_cleared_scores)
	DreamLo.dreamlo_getting_scores.connect(_on_getting_scores)
	DreamLo.dreamlo_got_scores.connect(_on_got_scores)
	
#	DreamLo.connect(DreamLo.DREAMLO_CLEARING_SCORES, self, "_on_clearing_scores")
#	DreamLo.connect(DreamLo.DREAMLO_CLEARED_SCORES, self, "_on_cleared_scores")
#	DreamLo.connect(DreamLo.DREAMLO_GETTING_SCORES, self, "_on_getting_scores")
#	DreamLo.connect(DreamLo.DREAMLO_GOT_SCORES, self, "_on_got_scores")
