@tool
extends EditorPlugin


func _enter_tree():
	add_autoload_singleton("DreamLo", "res://addons/gs_dreamlo/dreamlo.gd")
	
	
func _exit_tree():
	remove_autoload_singleton("DreamLo")
