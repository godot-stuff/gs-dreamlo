class_name DreamLoSettingsResource
extends Resource

@export var uri : String = "https://www.dreamlo.com/lb"
@export var private_code : String ="private"
@export var public_code : String = "public"
@export var url : String = "https://www.dreamlo.com/lb/private"
