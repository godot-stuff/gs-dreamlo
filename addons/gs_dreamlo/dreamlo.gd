#
# Copyright 2018-2021 SpockerDotNet LLC, All Rights Reserved.
#

extends Node

const DREAMLO_ADDING_SCORE = "dreamlo_adding_score"
const DREAMLO_ADDED_SCORE = "dreamlo_added_score"
const DREAMLO_REMOVING_SCORE = "dreamlo_removing_score"
const DREAMLO_REMOVED_SCORE = "dreamlo_removed_score"
const DREAMLO_GETTING_SCORES = "dreamlo_getting_scores"
const DREAMLO_GOT_SCORES = "dreamlo_got_scores"
const DREAMLO_CLEARING_SCORES = "dreamlo_clearing_scores"
const DREAMLO_CLEARED_SCORES = "dreamlo_cleared_scores"

signal dreamlo_adding_score(player, score)
signal dreamlo_added_score(player, score)
signal dreamlo_removing_player(player)
signal dreamlo_removed_player(player)
signal dreamlo_getting_scores()
signal dreamlo_got_scores(scores)
signal dreamlo_clearing_scores()
signal dreamlo_cleared_scores()
signal dreamlo_settings_loaded()

signal http_request_completed()


const DREAMLO_PLAYER_ADD_SCORE = "add/{player}/{score}"
const DREAMLO_PLAYER_ADD_SCORE_TIME = "add/{player}/{score}/{time}"
const DREAMLO_PLAYER_ADD_SCORE_TIME_MESSAGE = "add/{player}/{score}/{time}/{message}"
const DREAMLO_PLAYER_DELETE = "delete/{player}"
const DREAMLO_CLEAR_SCORES = "clear"
const DREAMLO_GET_SCORES = "{format}"
const DREAMLO_GET_SCORES_BY_SECONDS = "{format}-seconds-asc"
const DREAMLO_GET_SCORES_TOP = "{format}/{top}"
const DREAMLO_GET_SCORES_RANGE = "{format}/{start}/{count}"
const DREAMLO_GET_SCORES_PLAYER = "{format}-get/{player}"

enum ResponseTypes \
	{
		CLEAR,
		ADD,
		GET,
		DELETE,
	}

#var dreamlo_uri
#var dreamlo_url
#var dreamlo_private_code
#var dreamlo_public_code

var http_request

var response_type_queue = []

var request_queue = []

var settings : DreamLoSettingsResource
var version : String = "master"


func load_settings(filename: String):
	_load_settings(filename)

func add_score(player, score, time):
	Logger.trace("[DreamLo] add_score")
	response_type_queue.append(ResponseTypes.ADD)
	_submit_request(_build_dreamlo_request(DREAMLO_PLAYER_ADD_SCORE_TIME.format({ "player": player, "score": score, "time": time })))


func clear_scores():
	Logger.trace("[DreamLo] clear_scores")
	response_type_queue.append(ResponseTypes.CLEAR)
	emit_signal(DREAMLO_CLEARING_SCORES)
	_submit_request(_build_dreamlo_request(DREAMLO_CLEAR_SCORES))


func get_scores():
	Logger.trace("[DreamLo] get_scores")
	response_type_queue.append(ResponseTypes.GET)
	_submit_request(_build_dreamlo_request(DREAMLO_GET_SCORES.format({ "format": "json" })))


func get_scores_by_seconds():
	Logger.trace("[DreamLo] get_scores_by_seconds")
	response_type_queue.append(ResponseTypes.GET)
	_submit_request(_build_dreamlo_request(DREAMLO_GET_SCORES_BY_SECONDS.format({ "format": "json" })))


func remove_score(player, score):
	Logger.trace("dream_lo.remove_score")
	pass


func _build_dreamlo_request(command):
	Logger.trace("[DreamLo] _build_dream_request")
	return _build_request(settings.uri, settings.private_code, command)


func _build_request(uri, code, command):
	Logger.trace("[DreamLo] _build_request")
	Logger.debug("- uri: %s" % [uri])
	Logger.debug("- code: %s" % [code])
	Logger.debug("- command: %s" % [command])
	return "{uri}/{code}/{command}".format({ "uri": uri, "code": code, "command": command })

func _load_settings(config_filename : String):
	Logger.trace("[DreamLo] _load_settings")
	var _configuration = Configuration.new()
	var _data = _configuration.load_config(config_filename)
	settings = DreamLoSettingsResource.new() as DreamLoSettingsResource
	settings.uri = _data.get_value("dreamlo", "uri", settings.uri)
	settings.private_code = _data.get_value("dreamlo", "private_code", settings.private_code)
	settings.public_code = _data.get_value("dreamlo", "public_code", settings.public_code)
	settings.url = _data.get_value("dreamlo", "url", settings.url)
	emit_signal("dreamlo_settings_loaded")
	Logger.debug("- dreamlo settings loaded")

func _on_clear_response(response):
	Logger.trace("[DreamLo] _on_clear_response")
	emit_signal(DREAMLO_CLEARED_SCORES)


func _on_add_response(response):
	Logger.trace("[DreamLo] _on_add_response")


func _on_get_response(response):
	Logger.trace("[DreamLo] _on_get_response")
#	var data = parse_json(response.body.get_string_from_ascii())
	var data = JSON.parse_string(response.body.get_string_from_ascii())
	var dreamlo = data["dreamlo"]
	var leaderboard = dreamlo["leaderboard"]
	var entries = leaderboard["entry"]
	emit_signal(DREAMLO_GOT_SCORES, entries)


func _on_request_completed(result, code, headers, body):
	Logger.trace("[DreamLo] _on_request_completed")
	Logger.debug("- code: %s" % [code])
	emit_signal("http_request_completed", { "result": result, "code": code, "headers:": headers, "body": body})
	if !request_queue.is_empty():
		http_request.request(request_queue.pop_back())


func _on_http_request_completed(response):
	Logger.trace("[DreamLo] _on_http_request_completed")
	Logger.debug("- response: %s" % [response])
	Logger.debug("- body: %s" % [response.body.get_string_from_ascii()])

	var type = response_type_queue.pop_front()

	Logger.debug("- response type is: %s" % [type])

	match type:

		ResponseTypes.CLEAR:
			_on_clear_response(response)

		ResponseTypes.ADD:
			_on_add_response(response)

		ResponseTypes.GET:
			_on_get_response(response)


func _submit_request(request):
	Logger.trace("[DreamLo] _submit_request")
	Logger.debug("- request: %s" % [request])
	request_queue.push_front(request)
	if http_request.get_http_client_status() == HTTPClient.STATUS_DISCONNECTED:
		http_request.request(request_queue.pop_back())


func _ready():
	
	Logger.trace("[DreamLo] _ready")
	
	print()
	print("godot-stuff DreamLo")
	print("https://gitlab.com/godot-stuff/gs-dreamlo")
	print("Copyright 2018-2021, SpockerDotNet LLC")
	print("Version " + version)
	print()


func _init():
	
	Logger.trace("[DreamLo] _init")
	
	http_request = HTTPRequest.new()
	http_request.name = "DreamLo(HTTPRequest)"
	add_child(http_request)
	
	http_request.request_completed.connect(_on_request_completed)
	self.http_request_completed.connect(_on_http_request_completed)
	
#	http_request.connect("request_completed", self, "_on_request_completed")
#	self.connect("http_request_completed", self, "_on_http_request_completed")
